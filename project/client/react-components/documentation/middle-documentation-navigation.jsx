
'use strict';

import { ActionDocumentationCurrentNavigation } from '../../actions/action-documentation';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleDocumentationNavigation extends ReactComponentStore {
  constructor(props) {
    super(props, [props.documentationStore]);
    this.documentationStoreName = props.documentationStore.constructor.name;
  }
  
  _documentationStore(state) {
    return Reflect.get(state, this.documentationStoreName);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.titleText, nextProps.titleText)
      || !this.shallowCompare(this.props._uriPath, nextProps._uriPath)
      || !this.deepCompare(this._documentationStore(this.state).documentation.navigations, this._documentationStore(nextState).documentation.navigations)
      || !this.deepCompare(this._documentationStore(this.state).documentationPreview.navigations, this._documentationStore(nextState).documentationPreview.navigations)
      || !this.shallowCompare(this._documentationStore(this.state).current, this._documentationStore(nextState).current);
  }
  
  renderLinkInner(name, link) {
    if(!this.props.preview) {
      const match = name.match(/^\[\[stack\-[a-z-]+\]\]/);
      if(null !== match) {
        link = name;
        const oldName = name;
        name = oldName.substring('[[stack-'.length, match[0].length - 2) + oldName.substring(match[0].length);
      }
      return (
        <Link data-name={name} href={`/${link}`}>
          {name}
        </Link>
      );
    }
    else {
      return (
        <a className="doc_nav_preview">
          {name}
        </a>
      );
    }
  }
  
  renderLink(name, link, location, index) {
    const className = location === link ? "doc_nav_inner_chosen" : "doc_nav_inner";
    return (
      <li className={className} key={index}>
        <span className={className}></span>
        {this.renderLinkInner(name, link)}
      </li>
    );
  }
  
  renderPathInner(node, open, innerPaths) {
    const classNameUlInner = open ? "doc_nav_inner_heading_open" : "doc_nav_inner_heading_closed";
    return (
      <>
        <p data-name={node.name} className="doc_nav_inner_heading"
          onClickCapture={(e) => {
            e.stopPropagation();
            e.preventDefault();
            this.dispatch(this.props.documentationStore, new ActionDocumentationCurrentNavigation(this._documentationStore(this.state).current.navigationIndex, open ? node.parentPath : node.path, node.link));
          }}
        >{node.name}</p>
        <svg className={classNameUlInner} width="11" height="11" viewBox="0 0 11 11" 
          onClickCapture={(e) => {
            e.stopPropagation();
            e.preventDefault();
            this.dispatch(this.props.documentationStore, new ActionDocumentationCurrentNavigation(this._documentationStore(this.state).current.navigationIndex, open ? node.parentPath : node.path, node.link));
          }}
        >
          <polygon points="2,2 2,10 10,6"/>
        </svg>
        {innerPaths}
      </>
    );
  }
  
  renderPath(node, location, index) {
    const open = this._documentationStore(this.state).current.navigationPath.startsWith(node.path);
    const classNameUlInner = open ? "doc_nav_inner_heading_open" : "doc_nav_inner_heading_closed";
    let innerPaths = null;
    if(open) {
      innerPaths = (
        <ul className={classNameUlInner}>
          {this.renderTree(node.tree, location)}
       </ul>
      );
    }
    return (
      <li className="doc_nav_inner_heading" key={index}>
        <span className={classNameUlInner}></span>
        {this.renderPathInner(node, open, innerPaths)}
      </li>
    );
  }
  
  calc(names, path, link, tree) {
    if(1 === names.length) {
      const name = names[0];
      const thisPath = '' !== path ? `${path}/${name}` : name;
      const node = {
        name: name,
        path: thisPath,
        parentPath: path,
        link: link
      };
      tree.set(name, node);
    }
    else if(1 < names.length) {
      const name = names.shift();
      const thisPath = '' !== path ? `${path}/${name}` : name;
      if(tree.has(name)) {
        const node = tree.get(name);
        this.calc(names, thisPath, link, node.tree);
      }
      else {
        const node = {
          name: name,
          path: thisPath,
          parentPath: path,
          tree: new Map()
        };
        tree.set(name, node);
        this.calc(names, thisPath, link, node.tree);
      }
    }
  }
  
  renderTree(tree, location) {
    let index = 0;
    const nodes = [];
    if(undefined !== tree) {
      tree.forEach((node) => {
        if(node.link) {
          nodes.push(this.renderLink(node.name, node.link, location, index++));
        }
        else {
          nodes.push(this.renderPath(node, location, index++));
        }
      });
    }
    return nodes;
  }
  
  renderNavigations(refs, location, open, classNameUl) {
    if(!open) {
      return null;
    }
    const tree = new Map();
    refs.forEach((ref) => {
      const names = ref.name.split('/');
      this.calc(names, '', ref.link, tree);
    });
    return (
      <ul className={classNameUl}>
        {this.renderTree(tree, location)}
      </ul>
    );
  }
  
  _getNavigations() {
    if(!this.props.preview) {
      return this._documentationStore(this.state).documentation.navigations;
    }
    else {
      return this._documentationStore(this.state).documentationPreview.navigations;
    }
  }
  
  render() {
    const navigations = this._getNavigations();
    const location = undefined === this.props._uriPath ? '' : (this.props._uriPath.startsWith('/') ? this.props._uriPath.substring(1) : this.props._uriPath);
    const navigationSections = navigations.map((navigation, index) => {
      const open = this._documentationStore(this.state).current.navigationIndex === index;
      const classNameUl = open ? "doc_nav_heading_open" : "doc_nav_heading_closed";
      return (
        <div key={index} className="doc_nav_section">
          <span className={classNameUl}></span>
          <h3 data-name={navigation.heading} className="doc_nav_heading"
            onClickCapture={(e) => {
              e.stopPropagation();
              e.preventDefault();
              this.dispatch(this.props.documentationStore, new ActionDocumentationCurrentNavigation(index, '', ''));
            }}
          >{navigation.heading}</h3>
          <svg className={classNameUl} width="11" height="11" viewBox="0 0 11 11" 
            onClickCapture={(e) => {
              e.stopPropagation();
              e.preventDefault();
              this.dispatch(this.props.documentationStore, new ActionDocumentationCurrentNavigation(index, '', ''));
            }}
          >
            <polygon points="2,2 2,10 10,6"/>
          </svg>
          {this.renderNavigations(navigation.refs, location, open, classNameUl)}
        </div>
      );
    });
    const previewStyle = this.props.preview ? {position:'relative',width:'100%'} : null;
    return (
      <div className="doc_nav col-xs-2 panel panel-default" style={previewStyle}>
        <h3 className="doc_nav_title">{this.props.titleText}</h3>
        {navigationSections}
      </div>
    );
  }
}
