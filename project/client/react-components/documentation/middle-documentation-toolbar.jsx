
'use strict';

import { ActionDocumentationMarkup, ActionDocumentationMarkupSave, ActionDocumentationMarkupCancel, ActionDocumentationLocalNoteMarkupSave, ActionDocumentationLocalNoteMarkupCancel } from '../../actions/action-documentation';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';
import React from 'react';


export default class MiddleDocumentationToolbar extends ReactComponentStore {
  constructor(props) {
    super(props, [props.documentationStore], {
      guid: ''
    });
    this.boundKeyDown = this._keyDown.bind(this);
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
    this.documentationStoreName = props.documentationStore.constructor.name;
  }

  _documentationStore(state) {
    return Reflect.get(state, this.documentationStoreName);
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }

  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareObjectValues(this.props, nextProps)
      || !this.shallowCompare(this._documentationStore(this.state).markup, this._documentationStore(nextState).markup)
      || !this.shallowCompare(this._documentationStore(this.state).currentDocumentName, this._documentationStore(nextState).currentDocumentName)
      || !this.shallowCompare(this._documentationStore(this.state).localNoteMarkup, this._documentationStore(nextState).localNoteMarkup)
      || this.state.guid !== nextState.guid;
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _markupOpen() {
    this.dispatch(this.props.documentationStore, new ActionDocumentationMarkup());
  }
  
  _markupSave() {
    if(this._documentationStore(this.state).markup.definition) {
      this.dispatch(this.props.documentationStore, new ActionDocumentationMarkupSave());
    }
    else if(this._documentationStore(this.state).localNoteMarkup.definition) {
      this.dispatch(this.props.documentationStore, new ActionDocumentationLocalNoteMarkupSave());
    }
  }
  
  _markupHelp() {
  
  }
  
  _markupCancel() {
    if(this._documentationStore(this.state).markup.definition) {
      this.dispatch(this.props.documentationStore, new ActionDocumentationMarkupCancel());
    }
    else if(this._documentationStore(this.state).localNoteMarkup.definition) {
      this.dispatch(this.props.documentationStore, new ActionDocumentationLocalNoteMarkupCancel());
    }
  }
  
  _keyDown(e) {
    if(e.ctrlKey && 'o' === e.key) {
      if(!this.markupDisabledOpen) {
        e.preventDefault();
        this._markupOpen();
      }
    }
    else if(e.ctrlKey && e.shiftKey && '?' === e.key) {
      if(!this.markupDisabledHelp) {
        e.preventDefault();
        this._markupHelp();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      if(!this.markupDisabledCancel) {
        e.preventDefault();
        this._markupCancel();
      }
    }
    else if(e.ctrlKey && 's' === e.key) {
      if(!this.markupDisabledSave) {
        e.preventDefault();
        this._markupSave();
      }
    }
  }
  
  renderMarkupOpenButton() {
    this.markupDisabledOpen = this._documentationStore(this.state).markup.definition || this._documentationStore(this.state).localNoteMarkup.definition;
    return (
      <Button placement="bottom" heading="Open" content="Markup" shortcut="Ctrl+O" disabled={this.markupDisabledOpen}
        onClick={(e) => {
          this._markupOpen();
        }}
      >
        <span className="glyphicon glyphicon-edit" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupSaveButton() {
    this.markupDisabledSave = true;
    if(this._documentationStore(this.state).markup.definition) {
      this.markupDisabledSave = undefined !== this._documentationStore(this.state).markup.navigation.rows || (this._documentationStore(this.state).markup.navigation.content === this._documentationStore(this.state).markup.navigation.contentOriginal && this._documentationStore(this.state).markup.document.content === this._documentationStore(this.state).markup.document.contentOriginal);
    }
    else if(this._documentationStore(this.state).localNoteMarkup.definition) {
      this.markupDisabledSave = this._documentationStore(this.state).localNoteMarkup.document.content === this._documentationStore(this.state).localNoteMarkup.document.contentOriginal;
    }
    return (
      <Button placement="bottom" heading="Save" content="Markup" shortcut="Ctrl+S" disabled={this.markupDisabledSave}
        onClick={(e) => {
          this._markupSave();
        }}
      >
        <span className="glyphicon glyphicon-save-file" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupHelpButton() {
    this.markupDisabledHelp = this._documentationStore(this.state).markup.definition;
    return (
      <Button placement="bottom" heading="Help" content="Markup" shortcut="Ctrl+Shift+?" disabled={this.markupDisabledHelp}
        onClick={(e) => {
          this._markupHelp();
        }}
      >
        <span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupCancelButton() {
    this.markupDisabledCancel = !this._documentationStore(this.state).markup.definition && !this._documentationStore(this.state).localNoteMarkup.definition;
    return (
      <Button placement="bottom" heading="Cancel" content="Markup" shortcut="Ctrl+Shift+C" disabled={this.markupDisabledCancel}
        onClick={(e) => {
          this._markupCancel();
        }}
      >
        <span className="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupShowNavigationButton() {
    let toolTipInstruction = this.props.showNavigationMarkup ? "Hide" : "Show";
    let active = this.props.showNavigationMarkup ? "active" : "";
    return (
      <Button placement="bottom" heading={`${toolTipInstruction}`} content="Navigation Markup" active={active} disabled={!this._documentationStore(this.state).markup.definition}
        onClick={(e) => {
          this.props.onToggleNavigationMarkup && this.props.onToggleNavigationMarkup();
        }}
      >
        <span className="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
      </Button>
    );
  }

  renderMarkupShowPreviewNavigationButton() {
    const toolTipInstruction = this.props.showNavigationPreview ? "Hide" : "Show";
    const active = this.props.showNavigationPreview ? "active" : "";
    return (
      <Button placement="bottom" heading={`${toolTipInstruction}`} content="Navigation Preview" active={active} disabled={!this._documentationStore(this.state).markup.definition}
        onClick={(e) => {
          this.props.onToggleNavigationPreview && this.props.onToggleNavigationPreview();
        }}
      >
        <span className="glyphicon glyphicon-list-alt" aria-hidden="true" style={{top:'6px',left:'-7px',transform:'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-eye-open" aria-hidden="true" style={{width:'0px',top:'-1px',left:'-7px',transform:'scale(0.8)'}}></span>
      </Button>
    );
  }

  renderMarkupShowDocumentButton() {
    const toolTipInstruction = this.props.showDocumentationMarkup ? "Hide" : "Show";
    const active = this.props.showDocumentationMarkup ? "active" : "";
    return (
      <Button placement="bottom" heading={`${toolTipInstruction}`} content="Document Markup" active={active} disabled={!this._documentationStore(this.state).markup.definition}
        onClick={(e) => {
          this.props.onToggleDocumentationMarkup && this.props.onToggleDocumentationMarkup();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupShowDocumentPreviewButton() {
    const toolTipInstruction = this.props.showDocumentationPreview ? "Hide" : "Show";
    const active = this.props.showDocumentationPreview ? "active" : "";
    return (
      <Button placement="bottom" heading={`${toolTipInstruction}`} content="Document Preview" active={active} disabled={!this._documentationStore(this.state).markup.definition}
        onClick={(e) => {
          this.props.onToggleDocumentationPreview && this.props.onToggleDocumentationPreview();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top:'6px',left:'-7px',transform:'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-eye-open" aria-hidden="true" style={{width:'0px',top:'-1px',left:'-7px',transform:'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderHorizontalOrVerticalButton() {
    let toolTipInstruction;
    let image1Style;
    let image2Style;
    if(this.props.documentationOrder) {
      toolTipInstruction = 'Vertical';
      image1Style = {top:'8px',left:'0px',transform:'scale(1.3, 0.9)'};
      image2Style = {width:'0px',top:'-4px',left:'-14px',transform:'scale(1.3, 0.9)'};
    }
    else {
      toolTipInstruction = 'Horizontal';
      image1Style = {top:'2px',left:'-8px',transform:'scale(1.3, 0.9)'};
      image2Style = {width:'0px',top:'2px',left:'-6px',transform:'scale(1.3, 0.9)'};
    }
    return (
      <Button placement="bottom" heading={`${toolTipInstruction}`} content="Document Order" disabled={!this._documentationStore(this.state).markup.definition}
        onClick={(e) => {
          this.props.onToggleDocumentationOrder && this.props.onToggleDocumentationOrder();
        }}
      >
        <span className="glyphicon glyphicon-modal-window" aria-hidden="true" style={image1Style}></span>
        <span className="glyphicon glyphicon-modal-window" aria-hidden="true" style={image2Style}></span>
      </Button>
    );
  }

  renderGuidGeneratorButton() {
    if(this._documentationStore(this.state).markup.definition || this._documentationStore(this.state).localNoteMarkup.definition) {
      return (
        <Button placement="bottom" heading="Generate" content="GUID"
          onClick={(e) => {
            const guid = GuidGenerator.create();
            navigator.clipboard.writeText(guid).then(() => {
              /* clipboard successfully set */
            }, () => {
              /* clipboard write failed */
            });
            this.updateState({guid: {$set: guid}});
          }}
        >
          <span className="glyphicon glyphicon-barcode" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderGuidLabel(htmlFor) {
    if(this.state.guid && (this._documentationStore(this.state).markup.definition || this._documentationStore(this.state).localNoteMarkup.definition)) {
      return (
        <label className="documentation_guid" htmlFor={htmlFor}>{this.state.guid}</label>
      );
    }
  }
  
  renderDocumentationButton() {
    return (
      <Button id="from_doc_to_documentation" heading="Goto" content="Documentation"
        onClick={(e) => {
          this.context.history(`/documentation`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-book" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderEducationButton() {
    return (
      <Button id="from_doc_to_education" heading="Goto" content="Education"
        onClick={(e) => {
          this.context.history(`/education`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-education" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderWorkshopButton() {
    return (
      <Button id="from_doc_to_workshop" heading="Goto" content="Workshop"
        onClick={(e) => {
          this.context.history(`/workshop`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-leaf" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderVideosButton() {
    return (
      <Button id="from_doc_to_video" heading="Goto" content="Videos"
        onClick={(e) => {
          this.context.history(`/videos`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-film" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderLocalDocumentationButton(repoName) {
    return (
      <Button id={`from_start_to_local_documentation_${repoName}`} heading="Goto" content={`Documentation ${repoName}`}
        onClick={(e) => {
          this.context.history(`/local-documentation-${repoName}`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-book" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-pencil" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderLocalDocumentationButtons() {
    if(this.serviceExists('z-abs-servicelayer-docs-client')) {
       return this.renderLocalDocumentationButton('Local');
     }
  }
  
  render() {
    return (
      <div className="middle_toolbar middle_documentation_toolbar">
        <div className="btn-toolbar" role="toolbar" aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderMarkupOpenButton()}
            {this.renderMarkupSaveButton()}
            {this.renderMarkupHelpButton()}
            {this.renderMarkupCancelButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderMarkupShowDocumentButton()}
            {this.renderMarkupShowDocumentPreviewButton()}
            {this.renderMarkupShowNavigationButton()}
            {this.renderMarkupShowPreviewNavigationButton()}
            {this.renderHorizontalOrVerticalButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderGuidGeneratorButton()}
            {this.renderGuidLabel('aaa')}
          </div>
          <div className="btn-group btn-group-sm middle_documentation_heading" role="group" aria-label="...">
            <h4 className="middle_documentation_heading">{`${this.props.titleText} - ${this._documentationStore(this.state).current.navigationName}`}</h4>
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="..." style={{float: 'right'}}>
            {this.renderDocumentationButton()}
            {this.renderEducationButton()}
            {this.renderWorkshopButton()}
            {this.renderVideosButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="..." style={{float: 'right'}}>
            {this.renderLocalDocumentationButtons()}
          </div>
        </div>
      </div>
    );
  }
}


MiddleDocumentationToolbar.contextType = RouterContext;
