
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionDocumentationGet extends Action {
  constructor(documentName, embed, preview) {
    super(documentName, embed, preview);
  }
}

export class ActionDocumentationAdd extends Action {
  constructor() {
    super();
  }
}

export class ActionDocumentationUpdate extends Action {
  constructor() {
    super();
  }
}

export class ActionDocumentationNavigationsUpdate extends Action {
  constructor() {
    super();
  }
}

export class ActionDocumentationCurrentNavigation extends Action {
  constructor(index, path, link) {
    super(index, path, link);
  }
}

export class ActionDocumentationCurrentPage extends Action {
  constructor(documentName) {
    super(documentName);
  }
}

export class ActionDocumentationGotoPage extends Action {
  constructor(gotoStep) {
    super(gotoStep);
  }
}

export class ActionDocumentationMarkup extends Action {
  constructor() {
    super();
  }
}

export class ActionDocumentationMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionDocumentationMarkupSave extends Action {
  constructor() {
    super();
  }
}

export class ActionDocumentationMarkupNavigationChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionDocumentationMarkupDocumentChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionDocumentationLocalNoteMarkup extends Action {
  constructor(guid) {
    super(guid);
  }
}

export class ActionDocumentationLocalNoteMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionDocumentationLocalNoteMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionDocumentationLocalNoteMarkupSave extends Action {
  constructor() {
    super();
  }
}

export class ActionDocumentationLocalNoteGet extends Action {
  constructor(guid, preview) {
    super(guid, preview);
  }
}

export class ActionDocumentationLocalNoteAdd extends Action {
  constructor(guid, path) {
    super(guid, path);
  }
}

export class ActionDocumentationLocalNoteUpdate extends Action {
  constructor(guid, path) {
    super(guid, path);
  }
}

export class ActionDocumentationLocalNoteDelete extends Action {
  constructor(guid, path) {
    super(guid, path);
  }
}

export class ActionDocumentationReferencesGet extends Action {
  constructor(repo) {
    super(repo);
  }
}
