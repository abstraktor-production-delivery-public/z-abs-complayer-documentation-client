
'use strict';

import { DataActionDocumentationReferencesGet, DataActionDocumentationGet, DataActionDocumentationAdd, DataActionDocumentationUpdate, DataActionDocumentationNavigationsUpdate, DataActionDocumentationLocalNoteGet, DataActionDocumentationLocalNotesAndEmbededGet, DataActionDocumentationLocalNoteAdd, DataActionDocumentationLocalNoteUpdate, DataActionDocumentationLocalNoteDelete } from '../actions/data-action-documentation';
import MarkupDocumentationPage from 'z-abs-complayer-markup-client/client/markup/markup-documentation/markup-documentation-page';
import MarkupDocumentationNavigation from 'z-abs-complayer-markup-client/client/markup/markup-documentation/markup-documentation-navigation';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class DocumentationStoreBase extends StoreBaseData {
  constructor(documentationGroup, appName, repo) {
    super({
      markup: {
        definition: false,
        navigation: {
          content: undefined,
          contentOriginal: undefined,
          rows: undefined
        },
        document: {
          content: undefined,
          contentLines: 0,
          contentOriginal: undefined
        }
      },
      localNoteMarkup: {
        definition: false,
        document: {
          content: undefined,
          contentLines: 0,
          contentOriginal: undefined
        }
      },
      current: {
        navigationIndex: 0,
        navigationPath: '',
        navigationName: ''
      },
      currentDocumentName: undefined,
      currentLocalNoteGuid: undefined,
      documentation: {
        navigations: [],
        document: [],
        contentLines: 0,
        documentCreated: false,
        embededDocuments: new Map(),
        localNotes: new Map()
      },
      documentationPreview: {
        navigations: [],
        document: [],
        embededDocuments: new Map(),
        localNotes: new Map()
      },
      localNotePreview: {
        document: []
      },
      linkReferences: new Map()
    });
    this.documentationGroup = documentationGroup;
    this.appName = appName;
    this.repo = repo;
  }
  
  onInit() {
    this.sendDataAction(new DataActionDocumentationReferencesGet(this.repo));
  }
  
  onActionDocumentationCurrentNavigation(action) {
    this.updateState({current: {navigationIndex: {$set: action.index}}});
    this.updateState({current: {navigationPath: {$set: action.path}}});
    this.updateState({current: {navigationName: {$set: action.link}}});
  }
  
  onActionDocumentationCurrentPage(action) {
    if(this.state.markup.definition) {
      this._clearMarkup();
      this._clearDocumentationPreview();
    }
    if(this.state.localNoteMarkup.definition) {
      this._clearLocalNoteMarkup();
    }
    this.sendDataAction(new DataActionDocumentationGet(this.repo, this.appName, this.documentationGroup, action.documentName));
  }
  
  onActionDocumentationGotoPage(action) {
    const found = {
      nIndex: -1,
      rIndex: -1
    };
    const navigations = this.state.documentation.navigations;
    if('next' === action.gotoStep || 'previous' === action.gotoStep || 'next-section' === action.gotoStep || 'previous-section' === action.gotoStep) {
      for(let i = 0; i < navigations.length; ++i) {
        const refs = navigations[i].refs;
        for(let j = 0; j < refs.length; ++j) {
          if(this.state.currentDocumentName === refs[j].link) {
            found.nIndex = i;
            found.rIndex = j;
            break;
          }
        }
      }
      if(-1 !== found.nIndex && -1 != found.rIndex) {
        if('next' === action.gotoStep) {
          let startRefs = found.rIndex + 1;
          for(let i = found.nIndex; i < navigations.length; ++i) {
            const refs = navigations[i].refs;
            for(let j = startRefs; j < refs.length; ++j) {
              if('' !== refs[j].link) {
                return action.history(`/${refs[j].link}`, {replace: true});
              }
            }
            startRefs = 0;
          }
        }
        else if('previous' === action.gotoStep) {
          let startRefs = found.rIndex - 1;
          for(let i = found.nIndex; i >= 0; --i) {
            const refs = navigations[i].refs;
            for(let j = startRefs; j >= 0; --j) {
              if('' !== refs[j].link) {
                return action.history(`/${refs[j].link}`, {replace: true});
              }
            }
            if(i >= 1) {
              startRefs = navigations[i - 1].refs.length - 1;
            }
            else {
              startRefs = 0;
            }
          }
        }
        else if('next-section' === action.gotoStep) {
          let startRefs = 0;
          for(let i = found.nIndex + 1; i < navigations.length; ++i) {
            const refs = navigations[i].refs;
            for(let j = startRefs; j < refs.length; ++j) {
              if('' !== refs[j].link) {
                return action.history(`/${refs[j].link}`, {replace: true});
              }
            }
            startRefs = 0;
          }
        }
        else if('previous-section' === action.gotoStep) {
          let startRefs = 0;
          for(let i = found.nIndex - 1; i >= 0; --i) {
            const refs = navigations[i].refs;
            for(let j = startRefs; j > 0; --j) {
              if('' !== refs[j].link) {
                return action.history(`/${refs[j].link}`, {replace: true});
              }
            }
            if(i >= 1) {
              startRefs = navigations[i - 1].refs.length - 1;
            }
            else {
              startRefs = 0;
            }
          }
        }
      }
    }
    else if('first' === action.gotoStep) {
      for(let i = 0; i < navigations.length; ++i) {
        const refs = navigations[i].refs;
        for(let j = 0; j < refs.length; ++j) {
          if('' !== refs[j].link) {
            return action.history(`/${refs[j].link}`, {replace: true});
          }
        }
      }
    }
    else if('last' === action.gotoStep) {
      for(let i = navigations.length - 1; i >= 0; --i) {
        const refs = navigations[i].refs;
        for(let j = refs.length - 1; j >= 0; --j) {
          if('' !== refs[j].link) {
            return action.history(`/${refs[j].link}`, {replace: true});
          }
        }
      }
    }
  }
  
  onActionDocumentationMarkup(action) {
    const markupNavigation = MarkupDocumentationNavigation.stringify(this.state.documentation.navigations, this.state.currentDocumentName);
    const markupDocument = MarkupDocumentationPage.stringify(this.state.documentation.document);
    this.updateState({markup: {definition: {$set: true}}});
    this.updateState({markup: {navigation: {content: {$set: markupNavigation}}}});
    this.updateState({markup: {navigation: {contentOriginal: {$set: markupNavigation}}}});
    this.updateState({markup: {document: {content: {$set: markupDocument}}}});
    this.updateState({markup: {document: {contentLines: {$set: markupDocument.split('\n').length}}}});
    this.updateState({markup: {document: {contentOriginal: {$set: markupDocument}}}});
    this.updateState({documentationPreview: {navigations: {$set: this.state.documentation.navigations}}});
    this.updateState({documentationPreview: {document: {$set: this.state.documentation.document}}});
    
    this._getInternalDocumentationData(this.state.documentationPreview.document, this.state.documentationPreview.embededDocuments, true);
  }
  
  onActionDocumentationMarkupNavigationChange(action) {
    const result = MarkupDocumentationNavigation.parse(action.markup);
    this.updateState({markup: {navigation: {rows: {$set: result.rows}}}});
    if(result.success) {
      this.updateState({markup: {navigation: {content: {$set: action.markup}}}});
      this.updateState({documentationPreview: {navigations: {$set: result.navigations}}});
    }
  }
  
  onActionDocumentationMarkupDocumentChange(action) {
    this.updateState({markup: {document: {content: {$set: action.markup}}}});
    this.updateState({markup: {document: {contentLines: {$set: action.markup.split('\n').length}}}});
    const document = MarkupDocumentationPage.parse(action.markup);
    this.updateState({documentationPreview: {document: {$set: document}}});
    this._getInternalDocumentationData(document, this.state.documentationPreview.embededDocuments, true);
  }
  
  onActionDocumentationMarkupSave(action) {
    if(this.state.markup.navigation.contentOriginal !== this.state.markup.navigation.content && undefined === this.state.markup.navigation.rows) {
      const result = MarkupDocumentationNavigation.parse(this.state.markup.navigation.content);
      this.sendDataAction(new DataActionDocumentationNavigationsUpdate(this.repo, this.appName, this.documentationGroup, result.navigations));
    }
    if(this.state.documentation.documentCreated) {
      if(this.state.markup.document.contentOriginal !== this.state.markup.document.content) {
        this.sendDataAction(new DataActionDocumentationUpdate(this.repo, this.documentationGroup, this.state.currentDocumentName, this.state.markup.document.content));
      }
    }
    else {
      if(this.state.markup.document.contentOriginal !== this.state.markup.document.content) {
        this.sendDataAction(new DataActionDocumentationAdd(this.repo, this.documentationGroup, this.state.currentDocumentName, this.state.markup.document.content));
      }
    }
    this._clearMarkup();
    this._clearDocumentationPreview();
  }
  
  onActionDocumentationMarkupCancel(action) {
    this._clearMarkup();
    this._clearDocumentationPreview();
    this.sendDataAction(new DataActionDocumentationGet(this.repo, this.appName, this.documentationGroup, this.state.currentDocumentName));
  }

  onActionDocumentationLocalNoteMarkup(action) {
    const markupLocalNote = MarkupDocumentationPage.stringify(this.state.documentation.localNotes.get(action.guid).document);
    this.updateState({localNoteMarkup: {definition: {$set: true}}});
    this.updateState({localNoteMarkup: {document: {content: {$set: markupLocalNote}}}});
    this.updateState({localNoteMarkup: {document: {contentOriginal: {$set: markupLocalNote}}}});
    this.updateState({localNotePreview: {document: {$set: this.state.documentation.localNotes.get(action.guid).document}}});
    this.updateState({currentLocalNoteGuid: {$set: action.guid}});
  }

  onActionDocumentationLocalNoteMarkupChange(action) {
    this.updateState({localNoteMarkup: {document: {content: {$set: action.markup}}}});
    this.updateState({localNoteMarkup: {document: {contentLines: {$set: action.markup.split('\n').length}}}});
    const document = MarkupDocumentationPage.parse(action.markup);
    this.updateState({localNotePreview: {document: {$set: document}}});
  }

  onActionDocumentationLocalNoteMarkupSave(action) {
    let currentNote = this.state.documentation.localNotes.get(this.state.currentLocalNoteGuid);
    if(undefined !== currentNote) {
      if(currentNote.created) {
        this.sendDataAction(new DataActionDocumentationLocalNoteUpdate(this.state.currentLocalNoteGuid, this.state.currentDocumentName, this.state.localNoteMarkup.document.content));
      }
      else {
        this.sendDataAction(new DataActionDocumentationLocalNoteAdd(this.state.currentLocalNoteGuid, this.state.currentDocumentName, this.state.localNoteMarkup.document.content));
      }
    }
    this._clearLocalNoteMarkup();
  }
  
  onActionDocumentationLocalNoteMarkupCancel(action) {
    this._clearLocalNoteMarkup();
  }
  
  onActionDocumentationLocalNoteDelete(action) {
    this.sendDataAction(new DataActionDocumentationLocalNoteDelete(action.guid, this.state.currentDocumentName));
  }
  
  _calculateNavigationName(name) {
    const names = name.split('/');
    return names[names.length - 1];
  }
  
  onDataActionDocumentationReferencesGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({linkReferences: {$set: new Map(response.data)}});
    }
  }
  
  onDataActionDocumentationGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(!action.embed) {
        const document = MarkupDocumentationPage.parse(response.data[1]);
        this.updateState({documentation: {navigations: {$set: response.data[0]}}});
        this.updateState({documentation: {document: {$set: document}}});
        this.updateState({documentation: {contentLines: {$set: response.data[1].split('\n').length}}});
        this.updateState({documentation: {documentCreated: {$set: response.data[2]}}});
        if(null === action.documentName) {
          if(0 !== this.state.documentation.navigations.length) {
            const ref = this.state.documentation.navigations[0].refs[0];
            this.updateState({currentDocumentName: {$set: ref.link}});
            this.updateState({current: {navigationIndex: {$set: 0}}});
            this.updateState({current: {navigationPath: {$set: ref.name}}});
            this.updateState({current: {navigationName: {$set: this._calculateNavigationName(ref.name)}}});
          }
        }
        else {
          this.updateState({currentDocumentName: {$set: action.documentName}});
          const navigations = this.state.documentation.navigations;
          for(let i = 0; i < navigations.length; ++i) {
            const refs = navigations[i].refs;
            for(let j = 0; j < refs.length; ++j) {
              if(this.state.currentDocumentName === refs[j].link) {
                this.updateState({current: {navigationIndex: {$set: i}}});
                this.updateState({current: {navigationPath: {$set: refs[j].name}}});
                this.updateState({current: {navigationName: {$set: this._calculateNavigationName(refs[j].name)}}});
                break;
              }
            }
          }
        }
        this._getInternalDocumentationData(document, this.state.documentation.embededDocuments);
      }
      else {
        if(!response.data || 0 === response.data.length) {
          return;
        }
        const localNoteGuids = [];
        const funcLocalNotes = (document) => {
          document.filter((documentPart) => {
            return "markup_local_note" === documentPart.type;
          }).forEach((documentPart) => {
            localNoteGuids.push(documentPart.value.guid);
          });
          return document;
        };
        const responseDatas = response.data[0];
        const documentNames = response.data[1];
        let index = -1;
        if(!action.preview) {
          this.updateState({documentation: {embededDocuments: (embededDocuments) => {
            responseDatas.forEach((responseData) => {
              embededDocuments.set(documentNames[++index], funcLocalNotes(MarkupDocumentationPage.parse(responseData)));
            });
          }}});
        }
        else {
          this.updateState({documentationPreview: {embededDocuments: (embededDocuments) => {
            responseDatas.forEach((responseData) => {
              embededDocuments.set(documentNames[++index], funcLocalNotes(MarkupDocumentationPage.parse(responseData)));
            });
          }}});
        }
        this.sendDataAction(new DataActionDocumentationLocalNoteGet(localNoteGuids, this.state.currentDocumentName, action.preview));
      }
    }
    else {
      this.updateState({documentation: {navigations: {$set: []}}});
      this.updateState({documentation: {document: {$set: []}}});
    }
  }
  
  onDataActionDocumentationAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
    }
    this.sendDataAction(new DataActionDocumentationGet(this.repo, this.appName, this.documentationGroup, this.state.currentDocumentName));
  }
  
  onDataActionDocumentationUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
    }
    this.sendDataAction(new DataActionDocumentationGet(this.repo, this.appName, this.documentationGroup, this.state.currentDocumentName));
  }
  
  onDataActionDocumentationNavigationsUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
    }
    this.sendDataAction(new DataActionDocumentationGet(this.repo, this.appName, this.documentationGroup, this.state.currentDocumentName));
  }

  onDataActionDocumentationLocalNoteGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(response.data) {
        const responseDatas = Array.isArray(response.data) ? response.data : [response.data];
        if(!action.preview) {
          this.updateState({documentation: {localNotes: (localNotes) => {
            responseDatas.forEach((responseData) => {
              localNotes.set(responseData.guid, {
                document: MarkupDocumentationPage.parse(responseData.document),
                created: responseData.created
              });
            });
          }}});
        }
        else {
          this.updateState({documentationPreview: {localNotes: (localNotes) => {
            responseDatas.forEach((responseData) => {
              localNotes.set(responseData.guid, {
                document: MarkupDocumentationPage.parse(responseData.document),
                created: responseData.created
              });
            });
          }}});
        }
      }
    }
  }
  
  onDataActionDocumentationLocalNoteAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionDocumentationLocalNoteGet(action.guid, action.path));
    }
  }
  
  onDataActionDocumentationLocalNoteUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionDocumentationLocalNoteGet(action.guid, action.path));
    }
  }
  
  onDataActionDocumentationLocalNoteDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionDocumentationLocalNoteGet(action.guid, action.path));
    }
  }
  
  _clearMarkup() {
    this.updateState({markup: {definition: {$set: false}}});
    this.updateState({markup: {navigation: {content: {$set: undefined}}}});
    this.updateState({markup: {navigation: {contentOriginal: {$set: undefined}}}});
    this.updateState({markup: {navigation: {rows: {$set: undefined}}}});
    this.updateState({markup: {document: {content: {$set: undefined}}}});
    this.updateState({markup: {document: {contentLines: {$set: 0}}}});
    this.updateState({markup: {document: {contentOriginal: {$set: undefined}}}});
  }
  
  _clearLocalNoteMarkup() {
    this.updateState({localNoteMarkup: {definition: {$set: false}}});
    this.updateState({localNoteMarkup: {document: {content: {$set: undefined}}}});
    this.updateState({localNoteMarkup: {document: {contentLines: {$set: 0}}}});
    this.updateState({localNoteMarkup: {document: {contentOriginal: {$set: undefined}}}});
    this.updateState({currentLocalNoteGuid: {$set: undefined}});
  }
  
  _clearDocumentationPreview() {
    this.updateState({documentationPreview: {navigations: {$set: []}}});
    this.updateState({documentationPreview: {document: {$set: []}}});
    this.updateState({documentationPreview: {documentCreated: {$set: false}}});
    this.updateState({documentationPreview: {embededDocuments: {$set: new Map()}}});
    this.updateState({documentationPreview: {localNotes: {$set: new Map()}}});
  }
  
  _getInternalDocumentationData(document, embededDocuments, preview = false) {
    const localNoteGuids = [];
    document.filter((documentPart) => {
      return "markup_local_note" === documentPart.type;
    }).forEach((documentPart) => {
      localNoteGuids.push(documentPart.value.guid);
    });
    const documents = [];
    document.filter((documentPart) => {
      return "markup_embed" === documentPart.type;
    }).forEach((documentPart) => {
      if(!embededDocuments.has(documentPart.value.path)) { // RECURSION IS NOT ALLOWED
        documents.push(documentPart.value.path);
      }
    });
    this.sendDataAction(new DataActionDocumentationLocalNotesAndEmbededGet(localNoteGuids, this.state.currentDocumentName, preview, this.repo, this.appName, this.documentationGroup, documents, true, preview));
  }
}


module.exports = DocumentationStoreBase;
