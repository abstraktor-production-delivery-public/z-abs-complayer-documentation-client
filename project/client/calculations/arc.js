
'use strict';


class Arc {
  static describeArc(x, y, radius, spread, startAngle, endAngle) {
    const innerStart = this.polarToCartesian(x, y, radius, endAngle);
  	const innerEnd = this.polarToCartesian(x, y, radius, startAngle);
    const outerStart = this.polarToCartesian(x, y, radius + spread, endAngle);
    const outerEnd = this.polarToCartesian(x, y, radius + spread, startAngle);
    const largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";
    const d = [
        "M", outerStart.x, outerStart.y,
        "A", radius + spread, radius + spread, 0, largeArcFlag, 0, outerEnd.x, outerEnd.y,
        "L", innerEnd.x, innerEnd.y, 
        "A", radius, radius, 0, largeArcFlag, 1, innerStart.x, innerStart.y, 
        "L", outerStart.x, outerStart.y, "Z"
    ].join(" ");
    return d;
  }

  static polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    const angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;
    return {
      x: centerX + (radius * Math.cos(angleInRadians)),
      y: centerY + (radius * Math.sin(angleInRadians))
    };
  }
}

module.exports = Arc;
